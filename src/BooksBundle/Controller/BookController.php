<?php

namespace BooksBundle\Controller;

use BooksBundle\Entity\Book;
use BooksBundle\Entity\Author;
use BooksBundle\Form\BookType;
use BooksBundle\Service\BookService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BookController extends Controller
{
    /**
     * @Route("/index", name="books.book.index")
     */
    public function indexAction(BookService $bookService, Request $request)
    {
        $filterData = $request->query->all();
        $authors = $this->getDoctrine()->getRepository(Author::class)->findAll();
        $books = $this->getDoctrine()->getRepository(Book::class)->findByFilter($filterData);
        $categories = array_flip($bookService->getBooksCategories());

        return $this->render('@Books/Book/index.html.twig', array(
            'books' => $books,
            'categories' => $categories,
            'authors' => $authors
        ));
    }

    /**
     * @Route("/create", name="books.book.create")
     */
    public function createAction(BookService $bookService, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $book = new Book();

        $categories = $bookService->getBooksCategories();
        $form = $this->createForm(BookType::class, $book, [
            'books_categories' => $categories
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($book);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Book was created.');

            return $this->redirectToRoute('books.book.index');
        }

        return $this->render('@Books/Book/create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/update/{book_id}", name="books.book.update")
     */
    public function updateAction($book_id, Request $request, BookService $bookService)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($book_id);

        $categories = $bookService->getBooksCategories();

        $form = $this->createForm(BookType::class, $book, [
            'books_categories' => $categories
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($book);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Book was updated.');

            return $this->redirectToRoute('books.book.index');
        }
        return $this->render('@Books/Book/update.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/delete", name="books.book.delete")
     */
    public function deleteAction(Request $request)
    {
        $bookId = $request->request->get('book_id');
        if (!empty($bookId)) {

            $em = $this->getDoctrine()->getManager();
            $book = $em->getRepository(Book::class)->find($bookId);

            if ($book) {
                $em->remove($book);
                $em->flush();
            }
        }

        $request->getSession()->getFlashBag()->add('success', 'Book was removed.');

        return $this->redirectToRoute('books.book.index');
    }

}
