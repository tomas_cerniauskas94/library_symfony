<?php

namespace BooksBundle\Controller;

use BooksBundle\Entity\Author;
use BooksBundle\Form\AuthorType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AuthorController extends Controller
{
    /**
     * @Route("authors", name="books.author.index")
     */
    public function indexAction()
    {
        $authors = $this->getDoctrine()->getRepository(Author::class)->findAll();
        return $this->render('@Books/Author/index.html.twig', array(
            'authors' => $authors
        ));
    }

    /**
     * @Route("authors/create", name="books.author.create")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $author = new Author;

        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($author);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Author was created.');

            return $this->redirectToRoute('books.author.index');
        }

        return $this->render('@Books/Author/create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("authors/update/{author_id}", name="books.author.update")
     */
    public function updateAction($author_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $author = $em->getRepository(Author::class)->find($author_id);

        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($author);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Author was updated.');

            return $this->redirectToRoute('books.author.index');
        }
        return $this->render('@Books/Author/update.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("authors/delete", name="books.author.delete")
     */
    public function deleteAction(Request $request)
    {
        $authorId = $request->request->get('author_id');
        if (!empty($authorId)) {

            $em = $this->getDoctrine()->getManager();
            $author = $em->getRepository(Author::class)->find($authorId);

            if ($author) {
                $em->remove($author);
                $em->flush();
            }
        }

        $request->getSession()->getFlashBag()->add('success', 'Author was deleted.');

        return $this->redirectToRoute('books.author.index');
    }

}
