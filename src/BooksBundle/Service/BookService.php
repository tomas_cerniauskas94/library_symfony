<?php

namespace BooksBundle\Service;

class BookService
{
    public function getBooksCategories()
    {
        $categories = [
            'Cooking' => 0,
            'Art' => 1,
            'Fiction' => 2,
            'Computers' => 3
        ];

        return $categories;
    }
}