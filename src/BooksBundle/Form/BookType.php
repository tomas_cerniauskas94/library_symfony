<?php

namespace BooksBundle\Form;

use BooksBundle\Entity\Author;
use BooksBundle\Entity\Book;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Isbn;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class BookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $categories = $options['books_categories'];
        $builder
            ->add('name', TextType::class, [
                'label' => 'Title',
                'attr' => [
                    'class' => 'form-control',
                ],
                'constraints' => [new NotBlank()]
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Price',
                'attr' => [
                    'class' => 'form-control',
                ],
                'constraints' => [new NotBlank()]
            ])
            ->add('categoryId', ChoiceType::class, [
                'label' => 'Category',
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => $categories,
                'constraints' => [new NotBlank(), new Range(['min' => 0, 'max' =>3])]
            ])
            ->add('isbn', TextType::class, [
                'label' => 'ISBN',
                'attr' => [
                    'class' => 'form-control'
                ],
                'constraints' => [new NotBlank(), new Isbn(['type' => 'isbn10'])]
            ])
            ->add('authors', EntityType::class, [
                'class' => Author::class,
                'choice_label' => 'name',
                'multiple' => true,
                'attr' => [
                    'class' => 'form-control select2'
                ],
                'constraints' => [new NotBlank()]
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary btn-block'
                ]
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BooksBundle\Entity\Book'
        ));
        $resolver->setRequired(
            'books_categories'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'booksbundle_book';
    }


}
