$('[data-type=submit-delete-form]').click(function () {
    if (confirm('Do you want to delete?')) {
        $($(this).data('form')).submit();
    }
    return false;
});

$('.select2').select2();